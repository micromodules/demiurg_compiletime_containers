#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_containers__Version

#include <cstddef>

namespace ConstArrayHelper
{
    template<typename T_ArrayElementType, size_t kT_IndexToFill, typename ... T_ArgumentTypes>
    struct FillArrayExec;

    template<typename T_ArrayElementType, size_t kT_IndexToFill, typename T_ArgumentTypesHead, typename ... T_ArgumentTypesTail>
    struct FillArrayExec<T_ArrayElementType, kT_IndexToFill, T_ArgumentTypesHead, T_ArgumentTypesTail...>
    {
        static_assert(std::is_convertible<T_ArgumentTypesHead, T_ArrayElementType>::value, "Filling array elements should be convertivle");
        static constexpr void _(T_ArrayElementType* inOutArray,
                const T_ArgumentTypesHead& inHeadArgument,
                const T_ArgumentTypesTail& ... inTailArguments)
        {
            inOutArray[kT_IndexToFill] = inHeadArgument;
            FillArrayExec<T_ArrayElementType, kT_IndexToFill + 1, T_ArgumentTypesTail ...>::_(inOutArray, inTailArguments ...);
        }
    };

    template<typename T_ArrayElementType, size_t kT_IndexToFill, typename T_ArgumentTypesHead>
    struct FillArrayExec<T_ArrayElementType, kT_IndexToFill, T_ArgumentTypesHead>
    {
        static_assert(std::is_convertible<T_ArgumentTypesHead, T_ArrayElementType>::value, "Filling array elements should be convertivle");
        static constexpr void _(T_ArrayElementType* inOutArray,
                const T_ArgumentTypesHead& inHeadArgument)
        {
            inOutArray[kT_IndexToFill] = inHeadArgument;
        }
    };
}

// =====================================

template<typename T_Type, size_t kT_Size>
struct ConstexprArray
{
public:
    static_assert(kT_Size > 0, "Incorrect ConstArray size");

    constexpr ConstexprArray(const ConstexprArray<T_Type, kT_Size>& inArray)
        : _elements()
    {
        for(size_t theIndex = 0; theIndex < kT_Size; theIndex++)
            _elements[theIndex] = inArray._elements[theIndex];
    }

    template<typename ... T_ArgumentTypes>
    constexpr ConstexprArray(const T_ArgumentTypes& ... inArguments)
        : _elements()
    {
        ConstArrayHelper::FillArrayExec<T_Type, 0, T_ArgumentTypes ...>::_(_elements, inArguments ...);
    }

    constexpr size_t getSize() const { return kT_Size; }

    typedef const T_Type* const_iterator;
    constexpr const_iterator begin() const noexcept { return _elements; }
    constexpr const_iterator end() const noexcept { return _elements + getSize(); }

    constexpr const T_Type& operator[](const size_t inIndex){
        return _elements[inIndex];
    }

    constexpr bool areAllElementsEqualsTo(const T_Type inValue) const {
        for (const T_Type theElement : *this)
            if (inValue != theElement) return false;
        return true;
    }

private:
    T_Type _elements[kT_Size];
};

template<typename T_ArrayType, typename ... T_ArgumentTypes>
constexpr ConstexprArray<T_ArrayType, sizeof...(T_ArgumentTypes) + 1> makeConstArray(
        const T_ArrayType& inFirstArgument, const T_ArgumentTypes& ... inArguments)
{
    return { inFirstArgument, inArguments ... };
};

#endif
