#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_containers__Version

#include "demiurg_typescore/TypeCategory.hpp"
#include "demiurg_typescore/ConstTemplateValues.hpp"
#include "demiurg_diagnostic/Check.hpp"

namespace DOptionalDetails
{
    template<typename T_Type, ETypeCategory kT_TypeCatergory> struct TypeAdoption;

    template<typename T_Type> struct TypeAdoption<T_Type, ETypeCategory::Simple> {
        using APIType = T_Type;
        using StorageType = T_Type;
        constexpr static T_Type adoptAPIToStorage(T_Type inAPIValue) { return inAPIValue; }
        constexpr static T_Type adoptStorageToAPI(T_Type inStorageValue) { return inStorageValue; }
    };

    template<typename T_Type> struct TypeAdoption<T_Type&, ETypeCategory::LReference> {
        using APIType = T_Type&;
        using StorageType = T_Type*;
        constexpr static T_Type* adoptAPIToStorage(T_Type& inAPIValue) { return &inAPIValue; }
        constexpr static T_Type& adoptStorageToAPI(T_Type* inStorageValue) { return *inStorageValue; }
    };

    template<typename T_Type> struct TypeAdoption<const T_Type&, ETypeCategory::ConstLReference> {
        using APIType = const T_Type&;
        using StorageType = const T_Type*;
        constexpr static const T_Type* adoptAPIToStorage(const T_Type& inAPIValue) { return &inAPIValue; }
        constexpr static const T_Type& adoptStorageToAPI(const T_Type* inStorageValue) { return *inStorageValue; }
    };

    template<typename T_Type> struct TypeAdoption<T_Type&&, ETypeCategory::RReference> {
        static_assert(getAlwaysFalse<T_Type>(), "RValue references currently are not supported in DOptional");
    };

    template<typename T_Type> struct TypeAdoption<T_Type*, ETypeCategory::Pointer> {
        using APIType = T_Type*;
        using StorageType = T_Type*;
        constexpr static T_Type* adoptAPIToStorage(T_Type* inAPIValue) { return inAPIValue; }
        constexpr static T_Type* adoptStorageToAPI(T_Type* inStorageValue) { return inStorageValue; }
    };

    template<typename T_Type> struct TypeAdoption<const T_Type*, ETypeCategory::ConstPointer> {
        using APIType = const T_Type*;
        using StorageType = const T_Type*;
        constexpr static const T_Type* adoptAPIToStorage(const T_Type* inAPIValue) { return inAPIValue; }
        constexpr static const T_Type* adoptStorageToAPI(const T_Type* inStorageValue) { return inStorageValue; }
    };

    template<typename T_Type> struct TypeAdoption<T_Type, ETypeCategory::Object> {
        using APIType = const T_Type&;
        using StorageType = T_Type;
        constexpr static const T_Type& adoptAPIToStorage(const T_Type& inAPIValue) { return inAPIValue; }
        constexpr static const T_Type& adoptStorageToAPI(const T_Type& inStorageValueRef) { return inStorageValueRef; }
    };
}

template<typename T_Type>
class DOptional
{
public:
    using OwnType = DOptional<T_Type>;
    using TypeAdoption = DOptionalDetails::TypeAdoption<T_Type, GetTypeCategory<T_Type>::_>;
    using APIType = typename TypeAdoption::APIType;
    using StorageType = typename TypeAdoption::StorageType;

    constexpr DOptional(const OwnType& inValue) : _value(inValue._value), _isSet(inValue._isSet) { }
    constexpr DOptional(APIType inValue) : _value(inValue), _isSet(true) { }
    constexpr DOptional() : _value(), _isSet(false) { }

    constexpr bool isSet() const { return _isSet; }
    constexpr APIType getValue() const { return _value; }

    constexpr bool operator==(const OwnType& inOptional) const { return (_isSet == inOptional._isSet && _value == inOptional._value); }
    constexpr bool operator!=(const OwnType& inOptional) const { return !(inOptional == *this); }

private:
    union ValueWrapper {
    public:
        struct DummyInit{ };

        constexpr ValueWrapper(): asDummy{}{}
        constexpr ValueWrapper(APIType inValue): asValue{TypeAdoption::adoptAPIToStorage(inValue)}{}

        constexpr operator APIType() const { return TypeAdoption::adoptStorageToAPI(asValue); }
        constexpr bool operator ==(const ValueWrapper& inValueWrapper) { return (asValue == inValueWrapper.asValue); }

        ~ValueWrapper() = default;

    private:
        DummyInit asDummy;
        StorageType asValue;
    };

    ValueWrapper _value;
    bool _isSet{ false };
};

template<>
class DOptional<void>
{
public:
    using OwnType = DOptional<void>;

    constexpr DOptional(const OwnType& inValue) : _isSet(inValue._isSet) { }
    constexpr DOptional(const bool inIsSet = false) : _isSet(inIsSet) { }

    constexpr bool isSet() const { return _isSet; }

    constexpr bool operator==(const OwnType& inOptional) const { return (_isSet == inOptional._isSet); }
    constexpr bool operator!=(const OwnType& inOptional) const { return !(inOptional == *this); }

private:
    bool _isSet{ false };
};

#endif
