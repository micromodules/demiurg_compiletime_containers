#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_containers__Version

struct IDTypeInfo { };

template<typename T_Type>
struct DTypeInfo : public IDTypeInfo
{
public:
    using OwnType = DTypeInfo<T_Type>;

    constexpr DTypeInfo() { }

    DTypeInfo(const OwnType&) = delete;
    DTypeInfo(OwnType&&) = delete;
    OwnType& operator=(const OwnType&) = delete;
    OwnType& operator=(OwnType&&) = delete;
};

//NB: See details why we need this class here: https://stackoverflow.com/questions/29432283/c-static-constexpr-field-with-incomplete-type
template<typename T_Type>
struct DTypeInfoHolder
{
private:
    template<typename _T_Type>
    friend constexpr const DTypeInfo<_T_Type>& getTypeInfo();
    using TypeInfoType = DTypeInfo<T_Type>;
    constexpr static TypeInfoType sTypeInfoInstance{ };
};

template<typename T_Type>
constexpr typename DTypeInfoHolder<T_Type>::TypeInfoType DTypeInfoHolder<T_Type>::sTypeInfoInstance;

template<typename T_Type>
constexpr const DTypeInfo<T_Type>& getTypeInfo() {
    return DTypeInfoHolder<T_Type>::sTypeInfoInstance;
}

namespace DVariantHelper
{
    template<typename ... T_Types>
    struct GetMaxSizeOf;

    template<typename T_HeadType, typename ... T_TailTypes>
    struct GetMaxSizeOf<T_HeadType, T_TailTypes ...>
    {
        constexpr static size_t _() {
            constexpr size_t theHeadTypeSize = sizeof(T_HeadType);
            constexpr size_t theMaxTailTypesSize = sizeof(GetMaxSizeOf<T_TailTypes ...>::_());
            return (theHeadTypeSize > theMaxTailTypesSize ? theHeadTypeSize : theMaxTailTypesSize);
        }
    };

    template<typename T_HeadType>
    struct GetMaxSizeOf<T_HeadType>
    {
        constexpr static size_t _() {
            constexpr size_t theHeadTypeSize = sizeof(T_HeadType);
            return theHeadTypeSize;
        }
    };

    // ----------------

    template<typename T_TypeToConstructFrom, typename T_DefaultType, typename ... T_Types>
    struct GetTypeThatMayBeConstructedFrom;

    template<typename T_TypeToConstructFrom, typename T_DefaultType, typename T_HeadType, typename ... T_TailTypes>
    struct GetTypeThatMayBeConstructedFrom<T_TypeToConstructFrom, T_DefaultType, T_HeadType, T_TailTypes ...>
    {
        using _ = typename SelectType<std::is_constructible<T_HeadType, T_TypeToConstructFrom>::value,
            T_HeadType,
            typename GetTypeThatMayBeConstructedFrom<T_TypeToConstructFrom, T_DefaultType, T_TailTypes ...>::_>::_;
    };

    template<typename T_TypeToConstructFrom, typename T_DefaultType, typename T_HeadType>
    struct GetTypeThatMayBeConstructedFrom<T_TypeToConstructFrom, T_DefaultType, T_HeadType>
    {
        using _ = typename SelectType<std::is_constructible<T_HeadType, T_TypeToConstructFrom>::value,
            T_HeadType,
            T_DefaultType>::_;
    };

    // ----------------

    template<typename T_Type, typename ... T_Types>
    struct IsTypePresentedIn;

    template<typename T_Type, typename T_HeadType, typename ... T_TailTypes>
    struct IsTypePresentedIn<T_Type, T_HeadType, T_TailTypes ...>
    {
        constexpr static bool _() {
            constexpr bool theIsTypeSameAsHeadType = std::is_same<T_Type, T_HeadType>::value;
            return theIsTypeSameAsHeadType || IsTypePresentedIn<T_Type, T_TailTypes...>::_();
        }
    };

    template<typename T_Type, typename T_HeadType>
    struct IsTypePresentedIn<T_Type, T_HeadType>
    {
        constexpr static bool _() {
            constexpr bool theIsTypeSameAsHeadType = std::is_same<T_Type, T_HeadType>::value;
            return theIsTypeSameAsHeadType;
        }
    };
}

// ========================================================

template<typename ... T_VariantTypes>
class DVariant
{
public:
    template<typename T_ArgType>
    using SelectVariantType = DVariantHelper::GetTypeThatMayBeConstructedFrom<T_ArgType, std::false_type, T_VariantTypes ...>;

    template<typename T_VariantType>
    DVariant(const T_VariantType& inValueToCopy)
        : _data(),
          _type(getTypeInfo<typename SelectVariantType<T_VariantType>::_>())
    {
        using SelectedVariantType = typename SelectVariantType<T_VariantType>::_;
        static_assert(!std::is_same<SelectedVariantType, std::false_type>::value, "Unsupported type provided");
        new (&_data)SelectedVariantType(inValueToCopy);
    }

    template<typename T_VariantType>
    const T_VariantType* get() const {
        static_assert(DVariantHelper::IsTypePresentedIn<T_VariantType, T_VariantTypes ...>::_(), "Unsupported type provided");
        return (&_type == &getTypeInfo<T_VariantType>()) ? reinterpret_cast<const T_VariantType*>(_data) : nullptr;
    }

    template<typename T_VariantType>
    const T_VariantType& getOrReturnDefault() const {
        static const T_VariantType sDefault{ };
        const auto* theResult = get<T_VariantType>();
        return theResult ? *theResult : sDefault;
    }

private:
    constexpr static size_t kDataSize = DVariantHelper::GetMaxSizeOf<T_VariantTypes ...>::_();
    using DByteType = uint8_t;
    DByteType _data[kDataSize]{ };
    const IDTypeInfo& _type;
};

#endif
